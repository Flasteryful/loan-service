package com.loanservice.learning.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentPlanDto {
    private Long id;
    private Long creditId;
    private Boolean isClose;
    private LocalDate paymentDate;
    private Double creditBalanceAmount;
    private Double percentBalanceAmount;
    private Double fullCreditAmount;
    private Double factPayment;
    private LocalDate startDatePayment;
    private LocalDate endDatePayment;


}
