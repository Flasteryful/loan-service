package com.loanservice.learning.model.dto;

import lombok.Data;

@Data
public class MakeTransferDto {

    private String from;
    private String to;
    private Float amount;

}
