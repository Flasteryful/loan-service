package com.loanservice.learning.model.dto;


import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
//@RequiredArgsConstructor
public class AccountDto {
    private Long clientId;
    private Long accountNo;
    private AccountType credit;
    private ClientType retail;

    public AccountDto(Long clientId, AccountType credit, ClientType retail) {
    }
}
