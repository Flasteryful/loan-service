package com.loanservice.learning.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PercentDto {
    private LocalDate createDate;
    private Double percentRateCbr;
    private Double percentRateConsumerCredit;
    private Double percentRateAutoCredit;
    private Double percentRateMortgageCredit;
    private Double percentRateMicroloanCredit;

}
