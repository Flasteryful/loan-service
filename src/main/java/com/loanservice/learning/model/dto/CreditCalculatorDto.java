package com.loanservice.learning.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditCalculatorDto {
    private Long rateId;
    private LocalDate executionDate;
    private Double percentRate;
    private Integer creditTerm;
    private String creditType;
    private String stepPayment;
    private Double creditAmount;
}
