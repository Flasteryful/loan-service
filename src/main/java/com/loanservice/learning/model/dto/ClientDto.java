package com.loanservice.learning.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto {
    private Long id;
    private String citizenship;
    private Double incomePerMonth;
    private Double seniority;
    private Double creditRating;
    private String carBrand;
}
