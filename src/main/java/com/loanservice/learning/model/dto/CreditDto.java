package com.loanservice.learning.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditDto {
    private Long id;
    private Double percentRate;
    private Long account;
    private LocalDate createDate;
    private Double creditAmount;
    private Double percentAmount;
    private Integer creditTerm;
    private String creditType;
    private Double plannedMonthAmount;
    private Boolean isClosed;
}
