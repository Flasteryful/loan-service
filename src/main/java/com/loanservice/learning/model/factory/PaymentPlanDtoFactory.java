package com.loanservice.learning.model.factory;

import com.loanservice.learning.model.dto.PaymentPlanDto;
import com.loanservice.learning.model.entity.PaymentPlanEntity;
import org.springframework.stereotype.Component;

@Component
public class PaymentPlanDtoFactory {
    public PaymentPlanDto makePaymentPlanDto(PaymentPlanEntity paymentPlanEntity){
        return PaymentPlanDto.builder()
                .id(paymentPlanEntity.getId())
                .isClose(paymentPlanEntity.getIsClosed())
                .paymentDate(paymentPlanEntity.getPaymentDate())
                .creditBalanceAmount(paymentPlanEntity.getCreditBalanceAmount())
                .percentBalanceAmount(paymentPlanEntity.getPercentBalanceAmount())
                .factPayment(paymentPlanEntity.getFactPayment())
                .fullCreditAmount(paymentPlanEntity.getFullCreditAmount())
                .startDatePayment(paymentPlanEntity.getStartDatePayment())
                .endDatePayment(paymentPlanEntity.getEndDatePayment())
                .build();
    }
}
