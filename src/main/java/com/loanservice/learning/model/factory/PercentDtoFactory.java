package com.loanservice.learning.model.factory;

import com.loanservice.learning.model.dto.PercentDto;
import com.loanservice.learning.model.entity.PercentEntity;
import org.springframework.stereotype.Component;

@Component
public class PercentDtoFactory {
    public PercentDto makePercentDto(PercentEntity percentEntity){
        return PercentDto.builder()
                .createDate(percentEntity.getCreateDate())
                .percentRateCbr(percentEntity.getPercentRateCbr())
                .percentRateConsumerCredit(percentEntity.getPercentRateConsumerCredit())
                .percentRateAutoCredit(percentEntity.getPercentRateAutoCredit())
                .percentRateMortgageCredit(percentEntity.getPercentRateMortgageCredit())
                .percentRateMicroloanCredit(percentEntity.getPercentRateMicroloanCredit())
                .build();
    }
}
