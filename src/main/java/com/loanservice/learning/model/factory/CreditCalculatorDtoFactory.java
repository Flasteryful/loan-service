package com.loanservice.learning.model.factory;

import com.loanservice.learning.model.dto.CreditCalculatorDto;
import com.loanservice.learning.model.entity.CreditCalculatorEntity;
import org.springframework.stereotype.Component;

@Component
public class CreditCalculatorDtoFactory {
    public CreditCalculatorDto makeCreditCalculatorDto(CreditCalculatorEntity creditCalculatorEntity){
        return CreditCalculatorDto.builder()
                .rateId(creditCalculatorEntity.getRateId())
                .percentRate(creditCalculatorEntity.getPercentRate())
                .creditTerm(creditCalculatorEntity.getCreditTerm())
                .creditType(creditCalculatorEntity.getCreditType())
                .stepPayment(creditCalculatorEntity.getStepPayment())
                .creditAmount(creditCalculatorEntity.getCreditAmount())
                .build();
    }
}
