package com.loanservice.learning.model.factory;

import com.loanservice.learning.model.dto.ClientDto;
import com.loanservice.learning.model.entity.ClientEntity;
import org.springframework.stereotype.Component;

@Component
public class ClientDtoFactory {
    public ClientDto makeClientDto(ClientEntity clientEntity){
        return ClientDto.builder()
                .id(clientEntity.getId())
                .citizenship(clientEntity.getCitizenship())
                .incomePerMonth(clientEntity.getIncomePerMonth())
                .seniority(clientEntity.getSeniority())
                .creditRating(clientEntity.getCreditRating())
                .carBrand(clientEntity.getCarBrand())
                .build();
    }
}
