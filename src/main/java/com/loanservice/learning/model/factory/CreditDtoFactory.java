package com.loanservice.learning.model.factory;

import com.loanservice.learning.model.dto.CreditDto;
import com.loanservice.learning.model.entity.CreditEntity;
import org.springframework.stereotype.Component;

@Component
public class CreditDtoFactory {
    public CreditDto makeCreditDto(CreditEntity creditEntity){
        return CreditDto.builder()
                .id(creditEntity.getId())
                .percentRate(creditEntity.getPercentRate())
                .account(creditEntity.getAccount())
                .createDate(creditEntity.getCreateDate())
                .creditAmount(creditEntity.getCreditAmount())
                .percentAmount(creditEntity.getPercentAmount())
                .creditTerm(creditEntity.getCreditTerm())
                .creditType(creditEntity.getCreditType())
                .plannedMonthAmount(creditEntity.getPlannedMonthAmount())
                .isClosed(creditEntity.getIsClosed())
                .build();
    }
}
