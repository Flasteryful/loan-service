package com.loanservice.learning.model.entity;



import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "client")
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String citizenship;
    private Double incomePerMonth;
    private Double seniority;
    @Nullable
    private Double creditRating;
    @Nullable
    private String carBrand;

    @OneToMany(mappedBy = "clientEntity", fetch = FetchType.LAZY)
    private List<CreditEntity> creditEntities  = new ArrayList<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity clientEntity = (ClientEntity) o;
        return Objects.equals(id, clientEntity.id) && Objects.equals(citizenship, clientEntity.citizenship) && Objects.equals(incomePerMonth, clientEntity.incomePerMonth) && Objects.equals(seniority, clientEntity.seniority) && Objects.equals(creditRating, clientEntity.creditRating) && Objects.equals(carBrand, clientEntity.carBrand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, citizenship, incomePerMonth, seniority, creditRating, carBrand);
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "id=" + id +
                ", citizenship='" + citizenship + '\'' +
                ", incomePerMonth=" + incomePerMonth +
                ", seniority='" + seniority + '\'' +
                ", creditRating=" + creditRating +
                ", carBrand='" + carBrand + '\'' +
                '}';
    }
}
