package com.loanservice.learning.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "credit_calculator")
public class CreditCalculatorEntity {
    @Id
    @Column(name = "rate_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rateId;
    private Double percentRate;
    private Integer creditTerm;
    private String creditType;
    private String stepPayment;
    private Double creditAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    private PercentEntity percentEntity;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditCalculatorEntity that = (CreditCalculatorEntity) o;
        return Objects.equals(rateId, that.rateId) && Objects.equals(creditTerm, that.creditTerm) && Objects.equals(creditType, that.creditType) && Objects.equals(stepPayment, that.stepPayment) && Objects.equals(creditAmount, that.creditAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rateId, creditTerm, creditType, stepPayment, creditAmount);
    }

    @Override
    public String toString() {
        return "CreditCalculatorEntity{" +
                "id=" + rateId +
                ", executionDate=" +
                ", percentRate=" +
                ", creditTerm=" + creditTerm +
                ", creditType='" + creditType + '\'' +
                ", stepPayment='" + stepPayment + '\'' +
                ", creditAmount=" + creditAmount +
                '}';
    }
}
