package com.loanservice.learning.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "percent")
public class PercentEntity {
    @Id
    private LocalDate createDate;
    private Double percentRateCbr;
    private Double percentRateConsumerCredit;
    private Double percentRateAutoCredit;
    private Double percentRateMortgageCredit;
    private Double percentRateMicroloanCredit;
    @OneToMany(mappedBy = "percentEntity", fetch = FetchType.LAZY)
    private List<CreditCalculatorEntity> creditCalculatorEntities = new ArrayList<>();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PercentEntity percentEntity = (PercentEntity) o;
        return createDate.equals(percentEntity.createDate) && percentRateCbr.equals(percentEntity.percentRateCbr) && percentRateConsumerCredit.equals(percentEntity.percentRateConsumerCredit) && percentRateAutoCredit.equals(percentEntity.percentRateAutoCredit) && percentRateMortgageCredit.equals(percentEntity.percentRateMortgageCredit) && percentRateMicroloanCredit.equals(percentEntity.percentRateMicroloanCredit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createDate, percentRateCbr, percentRateConsumerCredit, percentRateAutoCredit, percentRateMortgageCredit, percentRateMicroloanCredit);
    }

    @Override
    public String toString() {
        return "PercentEntity{" +
                "createDate=" + createDate +
                ", percentRateCbr=" + percentRateCbr +
                ", percentRateConsumerCredit=" + percentRateConsumerCredit +
                ", percentRateAutoCredit=" + percentRateAutoCredit +
                ", percentRateMortgageCredit=" + percentRateMortgageCredit +
                ", percentRateMicroloanCredit=" + percentRateMicroloanCredit +
                '}';
    }
}
