package com.loanservice.learning.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "credit")
public class CreditEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private Double percentRate;
    private Long account;
    private LocalDate createDate;
    private Double creditAmount;
    private Double percentAmount;
    private Integer creditTerm;
    private String creditType;
    private Double plannedMonthAmount;
    private Boolean isClosed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "creditEntity", fetch = FetchType.LAZY)
    private List<PaymentPlanEntity> paymentPlanEntities = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditEntity creditEntity = (CreditEntity) o;
        return Objects.equals(id, creditEntity.id) && Objects.equals(account, creditEntity.account) && Objects.equals(createDate, creditEntity.createDate) && Objects.equals(creditAmount, creditEntity.creditAmount) && Objects.equals(percentAmount, creditEntity.percentAmount) && Objects.equals(creditTerm, creditEntity.creditTerm) && Objects.equals(creditType, creditEntity.creditType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, createDate, creditAmount, percentAmount, creditTerm, creditType);
    }

    @Override
    public String toString() {
        return "CreditEntity{" +
                "id=" + id +
                ", percentRate=" + percentRate +
                ", account=" + account +
                ", createDate=" + createDate +
                ", creditAmount=" + creditAmount +
                ", percentAmount=" + percentAmount +
                ", creditTerm=" + creditTerm +
                ", creditType='" + creditType + '\'' +
                ", plannedMonthAmount=" + plannedMonthAmount +
                ", isClosed=" + isClosed +
                ", clientEntity=" + clientEntity +
                '}';
    }
}
