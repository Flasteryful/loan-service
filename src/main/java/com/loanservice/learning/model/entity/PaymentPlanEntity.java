package com.loanservice.learning.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "payment_plan")
public class PaymentPlanEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long creditId;
    private Boolean isClosed;
    private LocalDate paymentDate;
    private Double creditBalanceAmount;
    private Double percentBalanceAmount;
    private Double fullCreditAmount;
    private Double factPayment;
    private LocalDate startDatePayment;
    private LocalDate endDatePayment;
    @ManyToOne(fetch = FetchType.LAZY)
    private CreditEntity creditEntity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentPlanEntity that = (PaymentPlanEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(paymentDate, that.paymentDate) && Objects.equals(creditBalanceAmount, that.creditBalanceAmount) && Objects.equals(percentBalanceAmount, that.percentBalanceAmount) && Objects.equals(startDatePayment, that.startDatePayment) && Objects.equals(endDatePayment, that.endDatePayment) && Objects.equals(creditEntity, that.creditEntity) && Objects.equals(factPayment, that.factPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paymentDate, creditBalanceAmount, creditBalanceAmount, startDatePayment, endDatePayment, creditEntity, factPayment);
    }

    @Override
    public String toString() {
        return "PaymentPlanEntity{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", creditBalanceAmount=" + creditBalanceAmount +
                ", percentBalanceAmount=" + percentBalanceAmount +
                ", factedPayment=" + factPayment +
                ", startDatePayment=" + startDatePayment +
                ", endDatePayment=" + endDatePayment +
                ", creditEntity=" + creditEntity +
                '}';
    }
}
