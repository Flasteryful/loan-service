package com.loanservice.learning.service;

import com.loanservice.learning.model.entity.PercentEntity;
import com.loanservice.learning.repository.PercentRepository;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Component
public class PercentServiceImpl implements PercentService {

    @Autowired
    private final PercentRepository percentRepository;

    public PercentServiceImpl(PercentRepository percentRepository) {
        this.percentRepository = percentRepository;
    }

    @Override
    public void loadPercentFromCbr() throws IOException {
        try {
            percentRepository.deleteAll();
            Document doc = Jsoup.connect("https://www.cbr.ru/hd_base/KeyRate/").get();

            Elements tableElements = doc.getElementsByAttributeValue("class", "data");

            tableElements.forEach(tableElement -> {

                Element tbodyElement = tableElement.child(0);
                Element trElement = tbodyElement.child(1);

                String percentParse = trElement.child(1).text();

                PercentEntity percentEntity = new PercentEntity();


                percentEntity.setCreateDate(LocalDate.now());


                percentParse = percentParse.replaceAll(",", ".");


                double percent = Double.parseDouble(percentParse);

                percentEntity.setPercentRateCbr(percent);
                percentEntity.setPercentRateAutoCredit(percent - 1);
                percentEntity.setPercentRateConsumerCredit(percent + 5);
                percentEntity.setPercentRateMortgageCredit(percent + 2);
                percentEntity.setPercentRateMicroloanCredit((percent / 10) * 30);

                percentRepository.save(percentEntity);

            });


        } catch (HttpStatusException ignore) {

        }
    }
}

