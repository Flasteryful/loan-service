package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.ClientDto;
import com.loanservice.learning.model.dto.CreditDto;

public interface CreditService {
    CreditDto createCredit(CreditDto creditDto, ClientDto clientDto);
}
