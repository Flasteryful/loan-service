package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.AccountDto;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class TransferClient {
    public String createTransfer(Long fromAccount, Long toAccount) {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put ("fromAccount", fromAccount);
            requestBody.put("toAccount", toAccount);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RestTemplate restTemplate = new RestTemplate();

        String resourceUrl
                = "http://localhost:8080/make-transfer";

        HttpEntity<String> request = new HttpEntity<>(requestBody.toString());

        String transferRepsonse = restTemplate
                .postForObject(resourceUrl, request, String.class);

        return transferRepsonse;
    }
}
