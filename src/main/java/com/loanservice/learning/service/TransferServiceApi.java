package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.MakeTransferDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TransferServiceApi implements TransferService {

    @Value("${service.transfer.url-make}")
    private String urlMakeTransfer;

    private final RestTemplate restTemplate;

    public TransferServiceApi(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public boolean makeTransfer(MakeTransferDto makeTransferDto) {
        ResponseEntity<Void> responseEntity = restTemplate.postForEntity(urlMakeTransfer, makeTransferDto, Void.class);
        return responseEntity.getStatusCode().is2xxSuccessful();
    }
}
