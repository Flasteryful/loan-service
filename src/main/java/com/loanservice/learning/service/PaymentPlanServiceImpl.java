package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.MakeTransferDto;
import com.loanservice.learning.model.entity.CreditEntity;
import com.loanservice.learning.model.entity.PaymentPlanEntity;
import com.loanservice.learning.repository.CreditRepository;
import com.loanservice.learning.repository.PaymentPlanRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Component
public class PaymentPlanServiceImpl implements PaymentPlanService {

    private static final Double ZERO = 0d;

    private final PaymentPlanRepository paymentPlanRepository;
    private final CreditRepository creditRepository;
    private final TransferService transferService;

    public PaymentPlanServiceImpl(PaymentPlanRepository paymentPlanRepository, CreditRepository creditRepository, TransferService transferService) {
        this.paymentPlanRepository = paymentPlanRepository;
        this.creditRepository = creditRepository;
        this.transferService = transferService;
    }

    @Override
    public void createPaymentPlan(Long creditId) {
CreditEntity creditEntity = creditRepository.findFirstById(creditId);

        Double factPayment = getFactPayment(
                creditEntity.getCreditAmount(),
                creditEntity.getPercentRate(),
                creditEntity.getCreditTerm());

        Double fullCreditAmount = factPayment * creditEntity.getCreditTerm();

        PaymentPlanEntity paymentPlanEntity = PaymentPlanEntity.builder()
                .creditId(creditId)
                .startDatePayment(creditEntity.getCreateDate())
                .endDatePayment(creditEntity.getCreateDate().plusMonths(creditEntity.getCreditTerm()))
                .isClosed(false)
                .creditBalanceAmount(creditEntity.getCreditAmount())
                .paymentDate(LocalDate.now())
                .percentBalanceAmount((factPayment * creditEntity.getCreditTerm())
                        - creditEntity.getCreditAmount())
                .factPayment(factPayment)
                .fullCreditAmount(fullCreditAmount)
                .creditEntity(creditEntity)
                .build();

        paymentPlanRepository.save(paymentPlanEntity);

    }


    @Override
    @Transactional
    public void updatePaymentPlanBasedOnCreditId(Long creditId) {
        MakeTransferDto makeTransferDto = new MakeTransferDto();

        PaymentPlanEntity lastPaymentPlan = paymentPlanRepository.findFirstByCreditIdOrderByPaymentDateDesc(creditId);

        CreditEntity creditEntity = creditRepository.findById(creditId).orElseThrow();

        Double factPayment = getFactPayment(
                lastPaymentPlan.getCreditBalanceAmount(),
                creditEntity.getPercentRate(),
                creditEntity.getCreditTerm()
        );

        Double fullCreditAmount = lastPaymentPlan.getFullCreditAmount() - factPayment;

        PaymentPlanEntity paymentPlanEntity = PaymentPlanEntity.builder()
                .creditId(creditId)
                .startDatePayment(lastPaymentPlan.getStartDatePayment())
                .endDatePayment(lastPaymentPlan.getEndDatePayment())
                .isClosed(ZERO.equals(lastPaymentPlan.getFullCreditAmount()))
                .creditBalanceAmount(lastPaymentPlan.getCreditBalanceAmount())
                .percentBalanceAmount(lastPaymentPlan.getPercentBalanceAmount())
                .paymentDate(LocalDate.now())
                .creditEntity(lastPaymentPlan.getCreditEntity())
                .fullCreditAmount(fullCreditAmount)
                .factPayment(factPayment)
                .build();


        makeTransferDto.setTo("12345");
        makeTransferDto.setFrom(creditEntity.getAccount().toString());
        makeTransferDto.setAmount(Float.parseFloat(factPayment.toString()));


        transferService.makeTransfer(makeTransferDto);

        paymentPlanRepository.save(paymentPlanEntity);
    }

    //Аннуитетная схема в 99 из 100 банков используют ее
    private static Double getFactPayment(Double creditAmount, Double percentRate, Integer creditTerm) {
        return creditAmount * ((percentRate / (100 * 12)) / ((1 - Math.pow((1 + (percentRate / (100 * 12))), (-creditTerm)))));
    }
}
