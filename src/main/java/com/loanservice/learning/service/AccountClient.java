package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.AccountDto;
import com.loanservice.learning.model.dto.AccountType;
import com.loanservice.learning.model.dto.ClientType;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AccountClient {
    public AccountDto createCreditAccount(Long clientId) {
        RestTemplate restTemplate = new RestTemplate();

        String resourceUrl
                = "http://localhost:8080/account";

        HttpEntity<AccountDto> request = new HttpEntity<>(
                new AccountDto(clientId, AccountType.CREDIT, ClientType.RETAIL));

        AccountDto accountRepsonse = restTemplate
                .postForObject(resourceUrl, request, AccountDto.class);

        return accountRepsonse;
    }
}
