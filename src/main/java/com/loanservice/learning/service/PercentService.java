package com.loanservice.learning.service;

import java.io.IOException;

public interface PercentService {
    void loadPercentFromCbr() throws IOException;
}
