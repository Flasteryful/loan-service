package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.MakeTransferDto;

public interface TransferService {

    boolean makeTransfer(MakeTransferDto makeTransferDto);

}
