package com.loanservice.learning.service;

import com.loanservice.learning.exception.BadRequestException;
import com.loanservice.learning.model.dto.AccountDto;
import com.loanservice.learning.model.dto.ClientDto;
import com.loanservice.learning.model.dto.CreditDto;
import com.loanservice.learning.model.entity.ClientEntity;
import com.loanservice.learning.model.entity.CreditCalculatorEntity;
import com.loanservice.learning.model.entity.CreditEntity;
import com.loanservice.learning.model.factory.CreditDtoFactory;
import com.loanservice.learning.repository.ClientRepository;
import com.loanservice.learning.repository.CreditCalculatorRepository;
import com.loanservice.learning.repository.CreditRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.DoubleStream;

@AllArgsConstructor
@Service
public class CreditServiceImpl implements CreditService {

    private CreditDtoFactory creditDtoFactory;
    private CreditCalculatorRepository creditCalculatorRepository;
    private ClientRepository clientRepository;
    private AccountClient accountClient;
    private TransferClient transferClient;
    private CreditRepository creditRepository;


    @Override
    @Transactional
    public CreditDto createCredit(CreditDto creditDto, ClientDto clientDto) {

        checkClient(clientDto);
        CreditEntity credit = new CreditEntity();
        //AccountDto account = accountClient.createCreditAccount(clientDto.getId());

        //credit.setAccount(account.getAccountNo());
        credit.setCreateDate(LocalDate.now());
        credit.setCreditAmount(creditDto.getCreditAmount());
        credit.setCreditTerm(creditDto.getCreditTerm());
        credit.setCreditType(creditDto.getCreditType());
        credit.setIsClosed(false);
        credit.setPercentRate(calculateFinalPercentCredit(creditDto));
        credit.setPercentAmount(creditDto.getCreditAmount() * credit.getPercentRate());
        credit.setPlannedMonthAmount(
                (creditDto.getCreditAmount() + credit.getPercentAmount()) / creditDto.getCreditTerm());
        credit.setClientEntity(clientRepository.getById(clientDto.getId()));

//        transferClient.createTransfer(GET BANK ACCOUNT, credit.getAccount());


        creditRepository.save(credit);
        return creditDtoFactory.makeCreditDto(credit);
    }

    private ClientEntity checkClient(ClientDto clientDto) {
        Optional<ClientEntity> clientEntity = clientRepository.findById(clientDto.getId());

        ClientEntity client;

        if (clientEntity.isEmpty()) {
            throw new BadRequestException("Client not found");
        } else {
            client = clientEntity.get();
        }
        if (client.getCreditRating() == 0) {
            throw new BadRequestException("Credit for client unaccessed");
        }
        return client;
    }

    private Double calculateFinalPercentCredit(CreditDto creditDto) {
        CreditCalculatorEntity credit = creditCalculatorRepository
                .getPercentRateByCreditAmountAndCreditTermAndCreditType(creditDto.getCreditAmount(),
                        creditDto.getCreditTerm(), creditDto.getCreditType());
        Double finalCreditPercent = getPercentRateWithCreditRating(credit.getPercentRate());
        return finalCreditPercent;
    }


    private Double getPercentRateWithCreditRating(Double creditPercent) {
        Double percentInsreace = DoubleStream.of(1.4, 2.3, 3.2, 4.1, 5.0)
                .filter(rate -> (rate - creditPercent) >= 0.0 && (rate - creditPercent) < 1.0)
                .map(rate -> rate - creditPercent)
                .sum();
        return percentInsreace;
    }
}
