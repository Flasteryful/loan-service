package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.CreditDto;

public interface PaymentPlanService {
    void createPaymentPlan(Long creditId);
    void updatePaymentPlanBasedOnCreditId(Long creditId);
}
