package com.loanservice.learning.service;

import com.loanservice.learning.model.entity.CreditCalculatorEntity;
import com.loanservice.learning.model.entity.PercentEntity;
import com.loanservice.learning.repository.CreditCalculatorRepository;
import com.loanservice.learning.repository.PercentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;


@Component

public class CreditCalculatorServiceImpl implements CreditCalculatorService {

    @Autowired
    private final CreditCalculatorRepository creditCalculatorRepository;


    @Autowired
    private final PercentRepository percentRepository;

    @Autowired
    private final PercentServiceImpl percentService;


    public CreditCalculatorServiceImpl(CreditCalculatorRepository creditCalculatorRepository, PercentRepository percentRepository, PercentServiceImpl percentService) {
        this.creditCalculatorRepository = creditCalculatorRepository;
        this.percentRepository = percentRepository;
        this.percentService = percentService;
    }


    @Override
    public void createCalculateCredit() throws IOException {
        percentService.loadPercentFromCbr();
        PercentEntity percentEntity = percentRepository.findByCreateDate(LocalDate.now());
        String stepPayment = "Месяц";


        creditCalculatorRepository.deleteAll();


        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                        .rateId(1L)
                .creditAmount(200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit())
                .creditTerm(6)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(2L)
                .creditAmount(200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 0.5)
                .creditTerm(12)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(3L)
                .creditAmount(200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 1)
                .creditTerm(24)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(4L)
                .creditAmount(500000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 1)
                .creditTerm(6)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(5L)
                .creditAmount(500000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 1.5)
                .creditTerm(12)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(6L)
                .creditAmount(500000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 2)
                .creditTerm(24)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(7L)
                .creditAmount(1000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 2)
                .creditTerm(6)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(8L)
                .creditAmount(1000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 2.5)
                .creditTerm(12)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(9L)
                .creditAmount(1000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateConsumerCredit() - 3)
                .creditTerm(24)
                .creditType("Потребительский")
                .percentEntity(percentEntity)
                .build());


        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(10L)
                .creditAmount(300000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit())
                .creditTerm(6)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(11L)
                .creditAmount(300000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 0.5)
                .creditTerm(12)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(12L)
                .creditAmount(300000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 1)
                .creditTerm(24)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(13L)
                .creditAmount(600000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 1)
                .creditTerm(6)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(14L)
                .creditAmount(600000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 1.5)
                .creditTerm(12)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(15L)
                .creditAmount(600000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 2)
                .creditTerm(24)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(16L)
                .creditAmount(1200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 2)
                .creditTerm(6)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(17L)
                .creditAmount(1200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 2.5)
                .creditTerm(12)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(18L)
                .creditAmount(1200000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateAutoCredit() - 3)
                .creditTerm(24)
                .creditType("Авто")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(19L)
                .creditAmount(2000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit())
                .creditTerm(24)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(20L)
                .creditAmount(2000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 0.5)
                .creditTerm(36)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(21L)
                .creditAmount(2000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 1)
                .creditTerm(48)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(22L)
                .creditAmount(4000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 1)
                .creditTerm(24)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(23L)
                .creditAmount(4000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 1.5)
                .creditTerm(36)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(24L)
                .creditAmount(4000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 2)
                .creditTerm(48)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(25L)
                .creditAmount(6000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 2)
                .creditTerm(24)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(26L)
                .creditAmount(6000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 2.5)
                .creditTerm(36)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(27L)
                .creditAmount(6000000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMortgageCredit() - 3)
                .creditTerm(48)
                .creditType("Ипотека")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(28L)
                .creditAmount(10000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit())
                .creditTerm(2)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(29L)
                .creditAmount(10000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 0.5)
                .creditTerm(4)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(30L)
                .creditAmount(10000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 1)
                .creditTerm(6)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(31L)
                .creditAmount(20000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 1)
                .creditTerm(2)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(32L)
                .creditAmount(20000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 1.5)
                .creditTerm(4)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(33L)
                .creditAmount(20000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 2)
                .creditTerm(6)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(34L)
                .creditAmount(30000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 2)
                .creditTerm(2)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(35L)
                .creditAmount(30000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 2.5)
                .creditTerm(4)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());

        creditCalculatorRepository.save(CreditCalculatorEntity.builder()
                .rateId(36L)
                .creditAmount(30000.0)
                .stepPayment(stepPayment)
                .percentRate(percentEntity.getPercentRateMicroloanCredit() - 3)
                .creditTerm(6)
                .creditType("Микрокредит")
                .percentEntity(percentEntity)
                .build());


    }
}
