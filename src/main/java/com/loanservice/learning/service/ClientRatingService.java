package com.loanservice.learning.service;

import com.loanservice.learning.model.dto.ClientDto;

public interface ClientRatingService {
    ClientDto createClient(ClientDto clientDto);
}
