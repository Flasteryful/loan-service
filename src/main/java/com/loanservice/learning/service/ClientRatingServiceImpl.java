package com.loanservice.learning.service;


import com.loanservice.learning.exception.BadRequestException;
import com.loanservice.learning.model.dto.ClientDto;
import com.loanservice.learning.model.entity.ClientEntity;
import com.loanservice.learning.model.factory.ClientDtoFactory;
import com.loanservice.learning.repository.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ClientRatingServiceImpl implements ClientRatingService {
    //кредитный рейтинг 0 - недоступен для нерезидентов, 1 - увеличить ставку на 40 %
    // 2 - увеличить ставку на 30 %, 3 - увеличить ставку на 20 %
    // 4 - увеличить ставку на 10 %, 5 - предложить базовую ставку по кредитному калькулятору

    private final ClientDtoFactory clientDtoFactory;

    private final ClientRepository clientRepository;


    public ClientDto createClient(ClientDto clientDto){
        clientRepository
                .findById(clientDto.getId())
                .ifPresent(client -> {
                    throw new BadRequestException("Client already exists id - " + clientDto.getId());
                });

        ClientEntity client = clientRepository.saveAndFlush(
                ClientEntity
                        .builder()
                        .id(clientDto.getId())
                        .citizenship(clientDto.getCitizenship())
                        .incomePerMonth(clientDto.getIncomePerMonth())
                        .seniority(clientDto.getSeniority())
                        .creditRating(getClientRating(
                                clientDto.getCitizenship(),
                                clientDto.getIncomePerMonth(),
                                clientDto.getSeniority(),
                                clientDto.getCarBrand()
                        ))
                        .carBrand(clientDto.getCarBrand())
                        .build()
        );

        return clientDtoFactory.makeClientDto(client);
    }



    private Double getClientRating(String citizenship, Double incomePerMonth,
                                  Double seniority, String carBrand) {
        Double clientRating;
        Double consumerBasket = 12000.0;

        if (!citizenship.equals("ru")) {
            clientRating = 0.0;
        } else {
            if (consumerBasket * 20 > incomePerMonth && seniority > 10.0 && carBrand.equals("notRus")) {
                clientRating = 5.0;
            } else if (consumerBasket * 10 > incomePerMonth && consumerBasket * 20 < incomePerMonth
                    && seniority > 5.0 && seniority < 10.0 && carBrand.equals("notRus")) {
                clientRating = 4.0;
            } else if (consumerBasket * 10 > incomePerMonth && consumerBasket * 20 < incomePerMonth
                    && seniority > 5.0 && seniority < 10.0 && carBrand.equals("rus")) {
                clientRating = 4.0;
            } else if (consumerBasket * 5 > incomePerMonth && consumerBasket * 10 < incomePerMonth
                    && seniority > 2.0 && seniority < 5.0 && carBrand.equals("rus")) {
                clientRating = 3.0;
            } else if (consumerBasket * 2 > incomePerMonth && consumerBasket * 5 < incomePerMonth
                    && seniority > 1.0 && seniority < 2.0 && carBrand.equals("rus")) {
                clientRating = 2.0;
            } else {
                clientRating = 1.0;
            }
        }
        return clientRating;
    }


}
