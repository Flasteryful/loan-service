package com.loanservice.learning.controller;

import com.loanservice.learning.model.dto.ClientDto;
import com.loanservice.learning.model.dto.CreditDto;
import com.loanservice.learning.model.entity.CreditEntity;
import com.loanservice.learning.repository.CreditRepository;
import com.loanservice.learning.service.ClientRatingService;
import com.loanservice.learning.service.CreditCalculatorServiceImpl;
import com.loanservice.learning.service.CreditService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;




@RequiredArgsConstructor
@RestController
public class Controller {

    private final ClientRatingService clientRatingService;

    private final CreditService creditService;

    private CreditRepository creditRepository;

    private final CreditCalculatorServiceImpl creditCalculatorService;


    public static final String CREATE_RATING = "/api/credit/rating/info";

    @PostMapping(CREATE_RATING)
    public ResponseEntity<String> createClient(@RequestBody Optional<ClientDto> clientDto) {
        if (clientDto.isEmpty()) {
            return ResponseEntity.badRequest().body("Client not found in request!");
        }
        ClientDto client = clientDto.get();
        clientRatingService.createClient(client);

        return ResponseEntity.ok().body("Client writed!");
    }


    public static final String OPEN_CREDIT = "/api/credit/new";

    @PostMapping(OPEN_CREDIT)
    public ResponseEntity<String> createCredit(@RequestBody Optional<CreditDto> creditDto,
                                               @RequestBody ClientDto clientDto) {
        if (creditDto.isEmpty()) {
            return ResponseEntity.badRequest().body("Information of credit not found in request!");
        }
        CreditDto credit = creditDto.get();
        creditService.createCredit(credit, clientDto);

        return ResponseEntity.ok().body("Credit writed!");
    }

    public static final String CLOSE_CREDIT = "/api/credit/close";

    @PostMapping(CLOSE_CREDIT)
    public ResponseEntity<String> closeCredit(@RequestBody Optional<CreditDto> creditDto) {

        CreditDto credit = creditDto.get();
        credit.setIsClosed(false);
        creditRepository.saveAndFlush(CreditEntity
                .builder()
                .isClosed(credit.getIsClosed())
                .build());

        return ResponseEntity.ok().body("Credit closed!");
    }


    private static final String CREDIT_CALCULATOR = "/api/credit/credit-calculator";

    @GetMapping(CREDIT_CALCULATOR)
    @Scheduled(cron = "0 0 0 * * *")
    public void creditCalculator() throws IOException {
        creditCalculatorService.createCalculateCredit();
    }
}
