package com.loanservice.learning.repository;

import com.loanservice.learning.model.entity.PercentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface PercentRepository extends JpaRepository<PercentEntity, LocalDate> {
    PercentEntity findByCreateDate(LocalDate createDate);
}
