package com.loanservice.learning.repository;

import com.loanservice.learning.model.entity.CreditEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CreditRepository extends JpaRepository<CreditEntity, Long> {
    CreditEntity findFirstById(Long creditId);


    CreditEntity getPercentRateById(Long creditId);

    CreditEntity getCreditTermById(Long creditId);

    CreditEntity getIsClosedById(Long creditId);

    CreditEntity getCreateDateById(Long creditId);

    CreditEntity getCreditAmountById(Long creditId);

}
