package com.loanservice.learning.repository;

import com.loanservice.learning.model.entity.PaymentPlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface PaymentPlanRepository extends JpaRepository<PaymentPlanEntity, Long> {

    PaymentPlanEntity findFirstByCreditIdOrderByPaymentDateDesc(Long creditId);



}
