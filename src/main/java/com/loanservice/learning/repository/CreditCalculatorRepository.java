package com.loanservice.learning.repository;

import com.loanservice.learning.model.entity.CreditCalculatorEntity;
import com.loanservice.learning.model.entity.CreditEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CreditCalculatorRepository extends JpaRepository<CreditCalculatorEntity, Long> {
    CreditCalculatorEntity getPercentRateByCreditAmountAndCreditTermAndCreditType(Double creditAmount,
                                                                          Integer creditTerm,
                                                                          String creditType);
}
